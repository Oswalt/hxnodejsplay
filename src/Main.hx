import js.node.Http;
import eweb.Dispatch;

class Main {

    static function route(req : js.node.http.IncomingMessage, res : js.node.http.ServerResponse)
    {
        trace(req.url);
        var raw = req.url.split("?");
            var base_url = raw[0];

            var params : Map<String, String> = new Map();

            if(raw[1] != null)
            for(p in raw[1].split("&"))
            {
                var str = StringTools.urlDecode(p);
                var p_key = str.split('=')[0];
                var p_value = str.split('=')[1];

                params.set(p_key, p_value);
            }

            try
            {
                var d = new Dispatch(base_url, params, req.method);
                
                d.dispatch(new Router(res));        
            }
            catch(e : eweb.DispatchError)
            {
                switch(e)
                {
                    case DETooManyValues:
                        trace("favicon.ico =/");
                    default:
                        throw e;
                }
            }
    }

    static function main() {
        var server = Http.createServer(route);

        server.listen(8080);
    }
}
